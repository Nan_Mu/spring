package org.example.config;

import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.example.constant.ZkClientConstants;
import org.example.task.ZkAdreessTask;
import org.example.utils.RedisUtils;
import org.example.zk.ZkNodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;


@Configuration()
public class ZkConfig
{
    private static final Logger logger = LoggerFactory.getLogger(ZkConfig.class);

    private final String PARENT_NODE = "/servers";
    @Autowired
    private ZkAdreessTask zkAdreessTask;
    @Value("${grpc.server.name}")
    private String nodePath;
    @Autowired
    private RedisUtils redisUtils;

    @Bean
    public ZooKeeper zkClient() throws IOException
    {
        System.out.println("prepare...");
        ZooKeeper zk = new ZooKeeper("127.0.0.1:2181", 30000, (event) -> {
            try
            {
                getServerList();
            }
            catch (Exception e)
            {
                throw new RuntimeException(e);
            }
            logger.info("监听到 watcher 通知：{}", event.toString());
        });
        System.out.println("complate connect..");
        return zk;
    }

    public void getServerList() throws InterruptedException, KeeperException, IOException
    {
        List<String> children = zkClient().getChildren(PARENT_NODE, true);

        for (String child : children)
        {
            System.out.println("节点:" + children + "发生了变化 从读取节点信息");
            String address = zkAdreessTask.queryNode(child);
            if (StringUtils.isNotBlank(address))
            {
                String old_adress = ZkNodes.getAdress(child);
                ZkNodes.addNode(child, address);

                //将节点信息保存在redis里
                redisUtils.hset(ZkClientConstants.server_node, child, address);

                System.out.println("---------------------------------------");
                System.out.println("节点:" + children + "发生了变化 old_node:" + old_adress + " new_node:" + address);
                System.out.println("---------------------------------------");
            }
        }
    }



}
