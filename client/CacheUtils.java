package org.example.utils;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.example.po.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <Description>
 *
 * @author: Mr.DengYuanFang
 * @project: spring-boot-grpc - zk - v2
 * @version: 1.0
 * @createDate: 2023/08/22 12:32
 */
@Component
public class CacheUtils
{

    @Autowired
    private CacheManager cacheManager;

    private static final Integer localCacheTimeout = 500;


    public void setLocalCache(String key, UserData userData, Integer timeout)
    {
        Cache cache = cacheManager.getCache("user");
        if (timeout == null)
        {
            timeout = localCacheTimeout;
        }
        if (userData != null)
        {
            Element element = new Element(key, userData);
            element.setTimeToLive(timeout);
            cache.put(element);
        }
    }

    public UserData getLocalCache(String key)
    {
        Cache cache = cacheManager.getCache("user");
        if (cache == null)
        {
            return null;
        }
        Element cacheElement = cache.get(key);
        if (cacheElement == null)
        {
            return null;
        }
        return (UserData) cacheElement.getObjectValue();
    }

    public Boolean delLocalCache(String key)
    {
        Cache cache = cacheManager.getCache("user");
        if (cache == null)
        {
            return null;
        }
        return cache.remove(key);
    }

}
