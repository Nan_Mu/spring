package org.example.zk;

import java.util.concurrent.ConcurrentHashMap;


public class ZkNodes
{
    private static ConcurrentHashMap<String,String> NODES = new ConcurrentHashMap();

    public static void addNode(String nodeName, String address)
    {
        NODES.put(nodeName, address);
    }

    public static String getAdress(String nodeName){
        return NODES.get(nodeName);
    }

}
