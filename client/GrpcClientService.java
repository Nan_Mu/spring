package org.example;

import com.alibaba.fastjson2.JSONObject;
import com.example.springbootgrpclib.grpc.protobuf.MyRequest;
import com.example.springbootgrpclib.grpc.protobuf.MyResponse;
import com.example.springbootgrpclib.grpc.protobuf.SimpleGrpc;
import io.grpc.*;
import io.grpc.stub.AbstractStub;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.example.common.Result;
import org.example.constant.ZkClientConstants;
import org.example.po.UserData;
import org.example.utils.CacheUtils;
import org.example.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.text.MessageFormat;
import java.util.Objects;

@Service
@Slf4j
public class GrpcClientService
{

    @Value("${grpc.server.name}")
    private String nodePath;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private CacheUtils cacheUtils;

    private final static String USER_INFO = "user_info";

    public Result oneToOne(int userId)
    {
        Result result = new Result();
        try
        {
            UserData userInfo = getUserInfo(String.valueOf(userId));

            if (Objects.nonNull(userInfo))
            {
                System.out.println("用户ID:" + userId + " 从本地获取成功");
                result.setData(userInfo);
                return result;
            }

            String service = (String) redisUtils.hget(ZkClientConstants.server_node, nodePath);

            ManagedChannel channel = ManagedChannelBuilder.forTarget(service).usePlaintext().build();
            SimpleGrpc.SimpleBlockingStub simpleStub1 = SimpleGrpc.SimpleBlockingStub.newStub(
                    new AbstractStub.StubFactory<SimpleGrpc.SimpleBlockingStub>()
                    {
                        @Override
                        public SimpleGrpc.SimpleBlockingStub newStub(Channel channel, CallOptions callOptions)
                        {
                            return new SimpleGrpc.SimpleBlockingStub(channel, callOptions);
                        }
                    }, channel);

            final MyResponse response = simpleStub1.oneToOne(
                    MyRequest.newBuilder().setUserId(String.valueOf(userId)).build());
            String resultStr = response.getMessage();

            if (StringUtils.isNotBlank(resultStr))
            {
                result = JSONObject.parseObject(resultStr, Result.class);
            }

            if (Objects.nonNull(result) && Objects.nonNull(result.getData()))
            {
                userInfo = JSONObject.parseObject(((JSONObject) result.getData()).toJSONString(), UserData.class);
                cacheUtils.setLocalCache(String.valueOf(userId), userInfo, 100);
                redisUtils.hset(USER_INFO, String.valueOf(userInfo.getUserId()), userInfo, 100-10);
                System.out.println("------SET CACHE-------");
                System.out.println("设置本次缓存e-cache成功");
                System.out.println("设置redis缓存成功");
                System.out.println("------SET CACHE-------");
            }

            return result;
        }
        catch (final StatusRuntimeException e)
        {
            return Result.fail("FAILED with " + e.getStatus().getCode().name());
        }
    }

    private UserData getUserInfo(String userId)
    {
       
        UserData userInfo = cacheUtils.getLocalCache(userId);
        if (Objects.isNull(userInfo))
        {
            
            userInfo = (UserData) redisUtils.hget(USER_INFO, String.valueOf(userId));
        }

        return userInfo;
    }

    private static UserData getData(int userIds)
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        Connection conn = null;
        PreparedStatement preparedstatement = null;
        ResultSet resultSet = null;
        try
        {
            String url = "jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf-8&useSSL=false&serverTimezone=UTC";
            String user = "root";
            String password = "975219";
            conn = DriverManager.getConnection(url, user, password);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("select * from t_user where user_id=");
            stringBuilder.append(userIds);
            String s = stringBuilder.toString();
            preparedstatement = conn.prepareStatement(s);
            resultSet = preparedstatement.executeQuery(); 
            while (resultSet.next())
            {
                int id = resultSet.getInt("id");
                int userId = resultSet.getInt("user_id");
                int status = resultSet.getInt("status");
                UserData userData = new UserData();
                userData.setId(id);
                userData.setUserId(userId);
                userData.setStatus(status);
                return userData;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String queryData(ZooKeeper zk, String nodePath) throws KeeperException, InterruptedException
    {
        System.out.println(MessageFormat.format("准备查询节点Data,path：{0}", nodePath));
        String data = new String(zk.getData(nodePath, false, queryStat(zk, nodePath)));
        System.out.println(MessageFormat.format("结束查询节点Data,path：{0}，Data：{1}", nodePath, data));
        return data;
    }

    public Stat queryStat(ZooKeeper zk, String nodePath) throws KeeperException, InterruptedException
    {
        System.out.println(MessageFormat.format("准备查询节点Stat，path：{0}", nodePath));
        Stat stat = zk.exists(nodePath, false);
        System.out.println(MessageFormat.format("结束查询节点Stat，path：{0}，version：{1}", nodePath, stat.getVersion()));
        return stat;
    }

}
