package org.example.task;

import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.example.utils.DateUtils;
import org.example.zk.ZkNodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import java.text.MessageFormat;


@Component
public class ZkAdreessTask
{
    private static final Logger logger = LoggerFactory.getLogger(ZkAdreessTask.class);

    @Autowired
    private ZooKeeper zk;

    @Value("${grpc.server.name}")
    private String nodePath;

    //10分钟一次   @Scheduled(cron="0 0/10 * * * ? *")

    @Scheduled(cron = "0/5 * * * * ?")
    public void execute()
    {
        logger.info("execute time={}", DateUtils.getTime());
        try
        {
            String address = queryNode(nodePath);
            if (StringUtils.isNotBlank(address))
            {
                logger.info("getNode 获取节点信息 time={},node=[{}]", DateUtils.getTime(), nodePath + "_" + address);
                ZkNodes.addNode(nodePath, address);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public String queryNode(@PathVariable("nodePath") String nodePath) throws KeeperException, InterruptedException
    {
        return queryData(zk, "/" + nodePath);
    }

    public String queryData(ZooKeeper zk, String nodePath) throws KeeperException, InterruptedException
    {
        System.out.println(MessageFormat.format("准备查询节点Data,path：{0}", nodePath));
        String data = new String(zk.getData(nodePath, false, queryStat(zk, nodePath)));
        System.out.println(MessageFormat.format("结束查询节点Data,path：{0}，Data：{1}", nodePath, data));
        return data;
    }

    public Stat queryStat(ZooKeeper zk, String nodePath) throws KeeperException, InterruptedException
    {
        System.out.println(MessageFormat.format("准备查询节点Stat，path：{0}", nodePath));
        Stat stat = zk.exists(nodePath, false);
        System.out.println(MessageFormat.format("结束查询节点Stat，path：{0}，version：{1}", nodePath, stat.getVersion()));
        return stat;
    }
}
