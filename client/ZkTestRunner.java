package org.example.zk;

import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class ZkTestRunner implements CommandLineRunner {

    @Autowired
    private CuratorFramework client;


    @Override
    public void run(String... args) throws Exception {

        Stat stat = client.checkExists().forPath("/zookeeper");
        if (stat != null) {
            System.out.println(stat.getAversion());
            System.out.println(stat.getCzxid());
            System.out.println(stat.getDataLength());
        }

        if (client.checkExists().forPath("/grpc") == null) {
            String s2 = client.create().forPath("/grpc");
            System.out.println(s2);
        }


        if (client.checkExists().forPath("/grpc/local_server") == null) {
            String s = client.create().forPath("/grpc/local_server");
            System.out.println(s);
        }

        if (client.checkExists().forPath("/grpc/local_client") == null) {
            String s1 = client.create().forPath("/grpc/local_client");
            System.out.println(s1);

        }

        if (client.checkExists().forPath("/dubbo/service/testService") == null) {

            String s3 = client.create().creatingParentsIfNeeded()
                              .withMode(CreateMode.EPHEMERAL)
                              .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE)
                              .forPath("/dubbo/service/testService");

            System.out.println(s3);
        }


        List<String> strings = client.getChildren().forPath("/");
        if (!CollectionUtils.isEmpty(strings)) {
            strings.forEach(System.out::println);
        }
    }
}
