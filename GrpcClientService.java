package org.example;

import com.alibaba.fastjson2.JSONObject;
import com.example.springbootgrpclib.grpc.protobuf.MyRequest;
import com.example.springbootgrpclib.grpc.protobuf.MyResponse;
import com.example.springbootgrpclib.grpc.protobuf.SimpleGrpc;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
//import org.example.grpc.Helloworld;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
public class GrpcClientService {

    @GrpcClient("local-grpc-server")
    private SimpleGrpc.SimpleBlockingStub simpleStub;

    public JSONObject oneToOne(final String nodePath) {

        try {
            //GRPC接口调用
            final MyResponse response = this.simpleStub.oneToOne(
                    MyRequest.newBuilder().setNodePath(nodePath)
                            .build());
            String message = response.getMessage();
            if(StringUtils.hasText(message)){
                return JSONObject.parseObject(message);
            }
        } catch (final StatusRuntimeException e) {
            e.fillInStackTrace();
        }
        return new JSONObject();
    }

    public String Calculator(final String a, final String b, final String type) {
        try {
            final MyResponse response1 = this.simpleStub.oneToOne(
                    MyRequest.newBuilder().setNodePath(a)
                            .setNodePath(b)
                            .setNodePath(type)
                            .build());
            String message = response1.getMessage();
            if(StringUtils.hasText(message)){
                return response1.getMessage();
            }

        }catch (final StatusRuntimeException e) {
            e.fillInStackTrace();
        }
        return new String();
    }
}