package com.example.congig;

import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;


@Configuration
public class ZkConfig {
    private static final Logger logger= LoggerFactory.getLogger(ZkConfig.class);

    @Bean
    public ZooKeeper create() throws IOException
    {
        System.out.println("prepare...");
        ZooKeeper zk = new ZooKeeper("127.0.0.1:2181", 30000, (event) -> {
            logger.info("监听到 watcher 通知：{}", event.toString());
        });
        System.out.println("complate connect..");
        return zk;
    }

}
