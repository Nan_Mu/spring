package com.example.controller;

import com.alibaba.fastjson2.JSONObject;
import com.example.entity.NodePath;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.MessageFormat;
import java.util.List;


@RestController
@RequestMapping(value = "/zk/node")
public class ZkNodeControler
{

    @Autowired
    private ZooKeeper zk;

    /*
        新增一个节点
        http://localhost:8087/zk/node/create
        {
          "nodeName": "userService",
          "address": "127.0.0.1:9091"
        }

        http://localhost:8087/zk/node/create
        {
          "nodeName": "orderService",
          "address": "127.0.0.1:9092"
        }
     */
    @PostMapping(value = "/create")
    public Boolean create(@RequestBody NodePath nodePath) throws KeeperException, InterruptedException
    {
        create(zk, "/" + nodePath.getNodeName(), nodePath.getAddress());
        return true;
    }

    @PostMapping(value = "/query/{nodePath}")
    public JSONObject queryNode(@PathVariable("nodePath") String nodePath) throws KeeperException, InterruptedException
    {
        JSONObject result = new JSONObject();
        String addr = queryData(zk, "/" + nodePath);
        result.put(nodePath, addr);
        return result;
    }

    void create(ZooKeeper zk,String nodePath,String nodeData) throws KeeperException, InterruptedException{
        System.out.println(MessageFormat.format("开始创建节点：{0}， 数据：{1}",nodePath,nodeData));
        List<ACL> acl = ZooDefs.Ids.OPEN_ACL_UNSAFE;
        CreateMode createMode = CreateMode.PERSISTENT;
        String result = zk.create(nodePath, nodeData.getBytes(), acl, createMode);
        System.out.println(MessageFormat.format("创建节点返回结果：{0}",result));
        System.out.println(MessageFormat.format("完成创建节点：{0}， 数据：{1}",nodePath,nodeData));
    }

    public Stat queryStat(ZooKeeper zk, String nodePath) throws KeeperException, InterruptedException
    {
        System.out.println(MessageFormat.format("准备查询节点Stat，path：{0}", nodePath));
        Stat stat = zk.exists(nodePath, false);
        System.out.println(MessageFormat.format("结束查询节点Stat，path：{0}，version：{1}", nodePath, stat.getVersion()));
        return stat;
    }

    public  String queryData(ZooKeeper zk,String nodePath) throws KeeperException, InterruptedException{
        System.out.println(MessageFormat.format("准备查询节点Data,path：{0}", nodePath));
        String data = new String(zk.getData(nodePath, false, queryStat(zk, nodePath)));
        System.out.println(MessageFormat.format("结束查询节点Data,path：{0}，Data：{1}", nodePath, data));
        return data;
    }


//    public  Stat update(ZooKeeper zk,String nodePath,String nodeData) throws KeeperException, InterruptedException{
//        Stat stat = queryStat(zk, nodePath);
//        System.out.println(MessageFormat.format("准备修改节点，path：{0}，data：{1}，原version：{2}", nodePath, nodeData, stat.getVersion()));
//        Stat newStat = zk.setData(nodePath, nodeData.getBytes(), stat.getVersion());
//        //修改节点值有两种方法，上面是第一种，还有一种可以使用回调函数及参数传递，与上面方法名称相同。
//        //zk.setData(path, data, version, cb, ctx);
//        System.out.println(MessageFormat.format("完成修改节点，path：{0}，data：{1}，现version：{2}", nodePath, nodeData, newStat.getVersion()));
//        return stat;
//    }
//
//
//    public void delete(ZooKeeper zk, String nodePath) throws InterruptedException, KeeperException {
//        //删除节点前先查询该节点信息
//        Stat stat = queryStat(zk, nodePath);
//        System.out.println(MessageFormat.format("准备删除节点，path：{0}，原version：{1}", nodePath, stat.getVersion()));
//        zk.delete(nodePath, stat.getVersion());
//        //修改节点值有两种方法，上面是第一种，还有一种可以使用回调函数及参数传递，与上面方法名称相同。
//        //zk.delete(path, version, cb, ctx);
//        System.out.println(MessageFormat.format("完成删除节点，path：{0}", nodePath));
//    }
}
