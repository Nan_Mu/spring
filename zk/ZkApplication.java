package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <Description>
 *
 * @author: Mr.DengYuanFang
 * @project: spring-boot-grpc
 * @version: 1.0
 * @createDate: 2023/07/22 16:42
 */
@SpringBootApplication
public class ZkApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZkApplication.class, args);
    }

}
