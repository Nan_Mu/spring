package com.example.demo.controller;

public class cal {
    private Integer a, b, type;

    public cal(Integer a, Integer b, Integer type) {
        this.a = a;
        this.b = b;
        this.type = type;
    }

    public Integer getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public Integer getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
