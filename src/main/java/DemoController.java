package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    //@GetMapping("/")
    public static void main(String[] args) {
//        //1、创建一个Spring的IOC容器对象
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        //2、从IOC容器中获取Bean实例
////        Calculate calculate = (Calculate) context.getBean("Calculate");
////        //3、调用sayHello()方法
////        calculate.Calculator2(1, 2, 1);
        //System.out.println(calculate(1, 2, 2));
    }

    @PostMapping("/calculate")
    public static String calculate(@RequestBody cal cal) {
//    public static String calculate(Integer a, Integer b, Integer type) {
        Integer result = 0;
        int a, b, type;
        a = cal.getA();
        b = cal.getB();
        type = cal.getType();

        switch (type) {
            case 1:
                result = a + b;
                break;
            case 2:
                result = a - b;
                break;
            case 3:
                result = a * b;
                break;
            case 4:
                result = a / b;
                break;
        }

        //cal cal = new cal(a, b, type);
        ResultJson resultJson = new ResultJson(result);
        return JSON.toJSONString(resultJson);
        //return JSON.toJSONString(cal)+ JSON.toJSONString(resultJson);
    }
}