package com.example.demo.controller;

public class ResultJson {
    private Integer result;

    public ResultJson(Integer result) {
        this.result = result;
    }

    public Integer getResult() {
        return result;
    }
}
