package org.example.congig;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.List;

//启动后自动注册节点local-server
@Component
public class ServiceRegister implements CommandLineRunner {

    @Autowired
    private ZooKeeper zk;

    @Value("${grpc.server.name}")
    private String nodePath;
    @Value("${grpc.server.port}")
    private String port;

    @Override
    public void run(String... args) throws Exception {
        create(zk, "/" + nodePath, "127.0.0.1:" + port);
    }

    void create(ZooKeeper zk,String nodePath,String nodeData) throws KeeperException, InterruptedException{
        System.out.println(MessageFormat.format("开始创建节点：{0}， 数据：{1}",nodePath,nodeData));
        List<ACL> acl = ZooDefs.Ids.OPEN_ACL_UNSAFE;
        CreateMode createMode = CreateMode.EPHEMERAL;
        String result = zk.create(nodePath, nodeData.getBytes(), acl, createMode);
        System.out.println(MessageFormat.format("创建节点返回结果：{0}",result));
        System.out.println(MessageFormat.format("完成创建节点：{0}， 数据：{1}",nodePath,nodeData));
    }
}
