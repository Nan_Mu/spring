package org.example;

import com.alibaba.fastjson2.JSONObject;
import com.example.springbootgrpclib.grpc.protobuf.MyRequest;
import com.example.springbootgrpclib.grpc.protobuf.MyResponse;
import com.example.springbootgrpclib.grpc.protobuf.SimpleGrpc;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import org.example.common.Result;
import org.example.congig.UserData;
import org.example.utils.BloomFilterUtil;
import org.example.utils.RedisUtils;
import org.redisson.api.RBloomFilter;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.sql.*;
import java.util.Objects;

@GrpcService
@Slf4j
public class GrpcServerService extends SimpleGrpc.SimpleImplBase {

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private BloomFilterUtil bloomFilterUtil;


    private RBloomFilter<Long> bloomFilter = null;

   
    @PostConstruct
    public void init() {
        bloomFilter = bloomFilterUtil.create();
    }


    @Override
    public void oneToOne(MyRequest request, StreamObserver<MyResponse> responseObserver) {
        //log.info("接收客户端数据{}", request);
        String userId = request.getUserId();
        Result result = new Result();
        
        if (bloomFilter.contains(Long.parseLong(userId)))
        {
            result = Result.fail("所要查询的数据既不在缓存中，也不在数据库中，为非法key");
            System.out.println("userId:" + userId + " 既不在缓存中，也不在数据库中，为非法key，，已过滤...");
        }
        else
        {
            UserData userData = getData(Long.parseLong(userId));

           
            if (Objects.isNull(userData))
            {
                Boolean addRes = bloomFilter.add(Long.parseLong(userId));
                if (addRes)
                {
                    System.out.println("userId:" + userId + "添加到布隆过滤器成功..");
                }
            }
            result = Result.ok(userData);
        }



        MyResponse response = MyResponse.newBuilder().setMessage(JSONObject.toJSONString(result))
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }


    private static UserData getData(Long userIds){
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection conn = null;
        PreparedStatement preparedstatement = null;
        ResultSet resultSet = null;
        try {
            String url = "jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf-8&useSSL=false&serverTimezone=UTC";
            String user = "root";
            String password = "975219";
            conn = DriverManager.getConnection(url, user, password);
            StringBuilder stringBuilder =new StringBuilder();
            stringBuilder.append("select * from t_user where user_id=");
            stringBuilder.append(userIds);
            String s = stringBuilder.toString();
            preparedstatement = conn.prepareStatement(s);
            resultSet = preparedstatement.executeQuery(); 
            while(resultSet.next()){
                long id = resultSet.getLong("id");
                int userId = resultSet.getInt("user_id");
                int status = resultSet.getInt("status");
                UserData userData =new UserData();
                userData.setId(id);
                userData.setUserId(userId);
                userData.setStatus(status);
                return userData;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
