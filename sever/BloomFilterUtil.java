package org.example.utils;

import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BloomFilterUtil
{
    @Resource
    private RedissonClient redissonClient;


    //过滤器名称
    @Value("${spring.redis.filterName}")
    private String filterName;

    //预测插入数量
    @Value("${spring.redis.expectedInsertions}")
    private Long expectedInsertions;

    //误判率
    @Value("${spring.redis.falsePositiveRate}")
    private Double falsePositiveRate;

    /**
     * 创建布隆过滤器
     *
     */
    public <T> RBloomFilter<T> create() {
        RBloomFilter<T> bloomFilter = redissonClient.getBloomFilter(filterName);
        bloomFilter.tryInit(expectedInsertions, falsePositiveRate);
        return bloomFilter;
    }
}
